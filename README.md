# JavaFX example Clickable

Simple JavaFX example to test Clickable build. 

## Build using Maven Client plugin

1. `mvn install`
2. `mvn javafx:compile`
3. `mvn javafx:run`
4. Verify that Application starts
5. [Download](https://github.com/graalvm/graalvm-ce-dev-builds/releases) the latest GraalVM development build for your platform and place in `/home/username/graalvm
6. Compile to native code: `mvn client:build`

## Build using [Clickable](https://gitlab.com/clickable/clickable)

See also the Clickable [Documentation](https://clickable-ut.dev/en/latest/)

1. Install Clickable: `pacaur -S clickable-git`
2. `clickable`

