#!/bin/bash

set -Eeuo pipefail

cp ${ROOT}/pom.xml ${BUILD_DIR}
cp -r ${ROOT}/src ${BUILD_DIR}
cd ${BUILD_DIR}

mvn install
mvn javafx:compile
